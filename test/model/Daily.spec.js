/**
 * Freqtrade API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 2021.9-dev10
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', process.cwd()+'/src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require(process.cwd()+'/src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.ftclient);
  }
}(this, function(expect, ftclient) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new ftclient.Daily();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('Daily', function() {
    it('should create an instance of Daily', function() {
      // uncomment below and update the code to test Daily
      //var instane = new ftclient.Daily();
      //expect(instance).to.be.a(ftclient.Daily);
    });

    it('should have the property data (base name: "data")', function() {
      // uncomment below and update the code to test the property data
      //var instance = new ftclient.Daily();
      //expect(instance).to.be();
    });

    it('should have the property fiatDisplayCurrency (base name: "fiat_display_currency")', function() {
      // uncomment below and update the code to test the property fiatDisplayCurrency
      //var instance = new ftclient.Daily();
      //expect(instance).to.be();
    });

    it('should have the property stakeCurrency (base name: "stake_currency")', function() {
      // uncomment below and update the code to test the property stakeCurrency
      //var instance = new ftclient.Daily();
      //expect(instance).to.be();
    });

  });

}));
