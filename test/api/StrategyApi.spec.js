/**
 * Freqtrade API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 2021.9-dev10
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', process.cwd()+'/src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require(process.cwd()+'/src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.ftclient);
  }
}(this, function(expect, ftclient) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new ftclient.StrategyApi();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('StrategyApi', function() {
    describe('getStrategyApiV1StrategyStrategyGet', function() {
      it('should call getStrategyApiV1StrategyStrategyGet successfully', function(done) {
        //uncomment below and update the code to test getStrategyApiV1StrategyStrategyGet
        //instance.getStrategyApiV1StrategyStrategyGet(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('listStrategiesApiV1StrategiesGet', function() {
      it('should call listStrategiesApiV1StrategiesGet successfully', function(done) {
        //uncomment below and update the code to test listStrategiesApiV1StrategiesGet
        //instance.listStrategiesApiV1StrategiesGet(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
  });

}));
