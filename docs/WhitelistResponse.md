# ftclient.WhitelistResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**whitelist** | **[String]** |  | 
**length** | **Number** |  | 
**method** | **[String]** |  | 


