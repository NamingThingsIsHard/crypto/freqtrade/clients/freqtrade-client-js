# ftclient.TradingApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**forcebuyApiV1ForcebuyPost**](TradingApi.md#forcebuyApiV1ForcebuyPost) | **POST** /api/v1/forcebuy | Forcebuy
[**forcesellApiV1ForcesellPost**](TradingApi.md#forcesellApiV1ForcesellPost) | **POST** /api/v1/forcesell | Forcesell
[**tradeApiV1TradeTradeidGet**](TradingApi.md#tradeApiV1TradeTradeidGet) | **GET** /api/v1/trade/{tradeid} | Trade
[**tradesApiV1TradesGet**](TradingApi.md#tradesApiV1TradesGet) | **GET** /api/v1/trades | Trades
[**tradesDeleteApiV1TradesTradeidDelete**](TradingApi.md#tradesDeleteApiV1TradesTradeidDelete) | **DELETE** /api/v1/trades/{tradeid} | Trades Delete



## forcebuyApiV1ForcebuyPost

> ForceBuyResponse forcebuyApiV1ForcebuyPost(forceBuyPayload)

Forcebuy

### Example

```javascript
import ftclient from 'freqtrade-client-js';
let defaultClient = ftclient.ApiClient.instance;
// Configure HTTP basic authorization: HTTPBasic
let HTTPBasic = defaultClient.authentications['HTTPBasic'];
HTTPBasic.username = 'YOUR USERNAME';
HTTPBasic.password = 'YOUR PASSWORD';
// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
let OAuth2PasswordBearer = defaultClient.authentications['OAuth2PasswordBearer'];
OAuth2PasswordBearer.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ftclient.TradingApi();
let forceBuyPayload = new ftclient.ForceBuyPayload(); // ForceBuyPayload | 
apiInstance.forcebuyApiV1ForcebuyPost(forceBuyPayload).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **forceBuyPayload** | [**ForceBuyPayload**](ForceBuyPayload.md)|  | 

### Return type

[**ForceBuyResponse**](ForceBuyResponse.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## forcesellApiV1ForcesellPost

> ResultMsg forcesellApiV1ForcesellPost(forceSellPayload)

Forcesell

### Example

```javascript
import ftclient from 'freqtrade-client-js';
let defaultClient = ftclient.ApiClient.instance;
// Configure HTTP basic authorization: HTTPBasic
let HTTPBasic = defaultClient.authentications['HTTPBasic'];
HTTPBasic.username = 'YOUR USERNAME';
HTTPBasic.password = 'YOUR PASSWORD';
// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
let OAuth2PasswordBearer = defaultClient.authentications['OAuth2PasswordBearer'];
OAuth2PasswordBearer.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ftclient.TradingApi();
let forceSellPayload = new ftclient.ForceSellPayload(); // ForceSellPayload | 
apiInstance.forcesellApiV1ForcesellPost(forceSellPayload).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **forceSellPayload** | [**ForceSellPayload**](ForceSellPayload.md)|  | 

### Return type

[**ResultMsg**](ResultMsg.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## tradeApiV1TradeTradeidGet

> OpenTradeSchema tradeApiV1TradeTradeidGet(tradeid)

Trade

### Example

```javascript
import ftclient from 'freqtrade-client-js';
let defaultClient = ftclient.ApiClient.instance;
// Configure HTTP basic authorization: HTTPBasic
let HTTPBasic = defaultClient.authentications['HTTPBasic'];
HTTPBasic.username = 'YOUR USERNAME';
HTTPBasic.password = 'YOUR PASSWORD';
// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
let OAuth2PasswordBearer = defaultClient.authentications['OAuth2PasswordBearer'];
OAuth2PasswordBearer.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ftclient.TradingApi();
let tradeid = 56; // Number | 
apiInstance.tradeApiV1TradeTradeidGet(tradeid).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tradeid** | **Number**|  | 

### Return type

[**OpenTradeSchema**](OpenTradeSchema.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## tradesApiV1TradesGet

> Object tradesApiV1TradesGet(opts)

Trades

### Example

```javascript
import ftclient from 'freqtrade-client-js';
let defaultClient = ftclient.ApiClient.instance;
// Configure HTTP basic authorization: HTTPBasic
let HTTPBasic = defaultClient.authentications['HTTPBasic'];
HTTPBasic.username = 'YOUR USERNAME';
HTTPBasic.password = 'YOUR PASSWORD';
// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
let OAuth2PasswordBearer = defaultClient.authentications['OAuth2PasswordBearer'];
OAuth2PasswordBearer.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ftclient.TradingApi();
let opts = {
  'limit': 500, // Number | 
  'offset': 0 // Number | 
};
apiInstance.tradesApiV1TradesGet(opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **Number**|  | [optional] [default to 500]
 **offset** | **Number**|  | [optional] [default to 0]

### Return type

**Object**

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## tradesDeleteApiV1TradesTradeidDelete

> DeleteTrade tradesDeleteApiV1TradesTradeidDelete(tradeid)

Trades Delete

### Example

```javascript
import ftclient from 'freqtrade-client-js';
let defaultClient = ftclient.ApiClient.instance;
// Configure HTTP basic authorization: HTTPBasic
let HTTPBasic = defaultClient.authentications['HTTPBasic'];
HTTPBasic.username = 'YOUR USERNAME';
HTTPBasic.password = 'YOUR PASSWORD';
// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
let OAuth2PasswordBearer = defaultClient.authentications['OAuth2PasswordBearer'];
OAuth2PasswordBearer.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ftclient.TradingApi();
let tradeid = 56; // Number | 
apiInstance.tradesDeleteApiV1TradesTradeidDelete(tradeid).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tradeid** | **Number**|  | 

### Return type

[**DeleteTrade**](DeleteTrade.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

