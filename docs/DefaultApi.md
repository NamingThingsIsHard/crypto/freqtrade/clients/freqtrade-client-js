# ftclient.DefaultApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**pingApiV1PingGet**](DefaultApi.md#pingApiV1PingGet) | **GET** /api/v1/ping | Ping



## pingApiV1PingGet

> Ping pingApiV1PingGet()

Ping

simple ping

### Example

```javascript
import ftclient from 'freqtrade-client-js';

let apiInstance = new ftclient.DefaultApi();
apiInstance.pingApiV1PingGet().then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters

This endpoint does not need any parameter.

### Return type

[**Ping**](Ping.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

