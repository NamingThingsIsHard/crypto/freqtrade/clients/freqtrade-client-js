# ftclient.Daily

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**[DailyRecord]**](DailyRecord.md) |  | 
**fiatDisplayCurrency** | **String** |  | 
**stakeCurrency** | **String** |  | 


