# ftclient.ForceSellPayload

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tradeid** | **String** |  | 
**ordertype** | [**OrderTypeValues**](OrderTypeValues.md) |  | [optional] 


