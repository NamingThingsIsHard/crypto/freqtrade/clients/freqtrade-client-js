# ftclient.StrategyResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**strategy** | **String** |  | 
**code** | **String** |  | 


