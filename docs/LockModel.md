# ftclient.LockModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | 
**active** | **Boolean** |  | 
**lockEndTime** | **String** |  | 
**lockEndTimestamp** | **Number** |  | 
**lockTime** | **String** |  | 
**lockTimestamp** | **Number** |  | 
**pair** | **String** |  | 
**reason** | **String** |  | 


