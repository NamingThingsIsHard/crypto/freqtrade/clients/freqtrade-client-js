# ftclient.OpenTradeSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tradeId** | **Number** |  | 
**pair** | **String** |  | 
**isOpen** | **Boolean** |  | 
**exchange** | **String** |  | 
**amount** | **Number** |  | 
**amountRequested** | **Number** |  | 
**stakeAmount** | **Number** |  | 
**strategy** | **String** |  | 
**buyTag** | **String** |  | [optional] 
**timeframe** | **Number** |  | 
**feeOpen** | **Number** |  | [optional] 
**feeOpenCost** | **Number** |  | [optional] 
**feeOpenCurrency** | **String** |  | [optional] 
**feeClose** | **Number** |  | [optional] 
**feeCloseCost** | **Number** |  | [optional] 
**feeCloseCurrency** | **String** |  | [optional] 
**openDate** | **String** |  | 
**openTimestamp** | **Number** |  | 
**openRate** | **Number** |  | 
**openRateRequested** | **Number** |  | [optional] 
**openTradeValue** | **Number** |  | 
**closeDate** | **String** |  | [optional] 
**closeTimestamp** | **Number** |  | [optional] 
**closeRate** | **Number** |  | [optional] 
**closeRateRequested** | **Number** |  | [optional] 
**closeProfit** | **Number** |  | [optional] 
**closeProfitPct** | **Number** |  | [optional] 
**closeProfitAbs** | **Number** |  | [optional] 
**profitRatio** | **Number** |  | [optional] 
**profitPct** | **Number** |  | [optional] 
**profitAbs** | **Number** |  | [optional] 
**profitFiat** | **Number** |  | [optional] 
**sellReason** | **String** |  | [optional] 
**sellOrderStatus** | **String** |  | [optional] 
**stopLossAbs** | **Number** |  | [optional] 
**stopLossRatio** | **Number** |  | [optional] 
**stopLossPct** | **Number** |  | [optional] 
**stoplossOrderId** | **String** |  | [optional] 
**stoplossLastUpdate** | **String** |  | [optional] 
**stoplossLastUpdateTimestamp** | **Number** |  | [optional] 
**initialStopLossAbs** | **Number** |  | [optional] 
**initialStopLossRatio** | **Number** |  | [optional] 
**initialStopLossPct** | **Number** |  | [optional] 
**minRate** | **Number** |  | [optional] 
**maxRate** | **Number** |  | [optional] 
**openOrderId** | **String** |  | [optional] 
**stoplossCurrentDist** | **Number** |  | [optional] 
**stoplossCurrentDistPct** | **Number** |  | [optional] 
**stoplossCurrentDistRatio** | **Number** |  | [optional] 
**stoplossEntryDist** | **Number** |  | [optional] 
**stoplossEntryDistRatio** | **Number** |  | [optional] 
**currentProfit** | **Number** |  | 
**currentProfitAbs** | **Number** |  | 
**currentProfitPct** | **Number** |  | 
**currentRate** | **Number** |  | 
**openOrder** | **String** |  | [optional] 


