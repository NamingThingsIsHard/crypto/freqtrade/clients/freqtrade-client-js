# ftclient.DeleteTrade

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cancelOrderCount** | **Number** |  | 
**result** | **String** |  | 
**resultMsg** | **String** |  | 
**tradeId** | **Number** |  | 


