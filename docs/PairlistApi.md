# ftclient.PairlistApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**blacklistApiV1BlacklistGet**](PairlistApi.md#blacklistApiV1BlacklistGet) | **GET** /api/v1/blacklist | Blacklist
[**blacklistDeleteApiV1BlacklistDelete**](PairlistApi.md#blacklistDeleteApiV1BlacklistDelete) | **DELETE** /api/v1/blacklist | Blacklist Delete
[**blacklistPostApiV1BlacklistPost**](PairlistApi.md#blacklistPostApiV1BlacklistPost) | **POST** /api/v1/blacklist | Blacklist Post
[**whitelistApiV1WhitelistGet**](PairlistApi.md#whitelistApiV1WhitelistGet) | **GET** /api/v1/whitelist | Whitelist



## blacklistApiV1BlacklistGet

> BlacklistResponse blacklistApiV1BlacklistGet()

Blacklist

### Example

```javascript
import ftclient from 'freqtrade-client-js';
let defaultClient = ftclient.ApiClient.instance;
// Configure HTTP basic authorization: HTTPBasic
let HTTPBasic = defaultClient.authentications['HTTPBasic'];
HTTPBasic.username = 'YOUR USERNAME';
HTTPBasic.password = 'YOUR PASSWORD';
// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
let OAuth2PasswordBearer = defaultClient.authentications['OAuth2PasswordBearer'];
OAuth2PasswordBearer.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ftclient.PairlistApi();
apiInstance.blacklistApiV1BlacklistGet().then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters

This endpoint does not need any parameter.

### Return type

[**BlacklistResponse**](BlacklistResponse.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## blacklistDeleteApiV1BlacklistDelete

> BlacklistResponse blacklistDeleteApiV1BlacklistDelete(opts)

Blacklist Delete

Provide a list of pairs to delete from the blacklist

### Example

```javascript
import ftclient from 'freqtrade-client-js';
let defaultClient = ftclient.ApiClient.instance;
// Configure HTTP basic authorization: HTTPBasic
let HTTPBasic = defaultClient.authentications['HTTPBasic'];
HTTPBasic.username = 'YOUR USERNAME';
HTTPBasic.password = 'YOUR PASSWORD';
// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
let OAuth2PasswordBearer = defaultClient.authentications['OAuth2PasswordBearer'];
OAuth2PasswordBearer.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ftclient.PairlistApi();
let opts = {
  'pairsToDelete': ["null"] // [String] | 
};
apiInstance.blacklistDeleteApiV1BlacklistDelete(opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pairsToDelete** | [**[String]**](String.md)|  | [optional] 

### Return type

[**BlacklistResponse**](BlacklistResponse.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## blacklistPostApiV1BlacklistPost

> BlacklistResponse blacklistPostApiV1BlacklistPost(blacklistPayload)

Blacklist Post

### Example

```javascript
import ftclient from 'freqtrade-client-js';
let defaultClient = ftclient.ApiClient.instance;
// Configure HTTP basic authorization: HTTPBasic
let HTTPBasic = defaultClient.authentications['HTTPBasic'];
HTTPBasic.username = 'YOUR USERNAME';
HTTPBasic.password = 'YOUR PASSWORD';
// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
let OAuth2PasswordBearer = defaultClient.authentications['OAuth2PasswordBearer'];
OAuth2PasswordBearer.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ftclient.PairlistApi();
let blacklistPayload = new ftclient.BlacklistPayload(); // BlacklistPayload | 
apiInstance.blacklistPostApiV1BlacklistPost(blacklistPayload).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blacklistPayload** | [**BlacklistPayload**](BlacklistPayload.md)|  | 

### Return type

[**BlacklistResponse**](BlacklistResponse.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## whitelistApiV1WhitelistGet

> WhitelistResponse whitelistApiV1WhitelistGet()

Whitelist

### Example

```javascript
import ftclient from 'freqtrade-client-js';
let defaultClient = ftclient.ApiClient.instance;
// Configure HTTP basic authorization: HTTPBasic
let HTTPBasic = defaultClient.authentications['HTTPBasic'];
HTTPBasic.username = 'YOUR USERNAME';
HTTPBasic.password = 'YOUR PASSWORD';
// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
let OAuth2PasswordBearer = defaultClient.authentications['OAuth2PasswordBearer'];
OAuth2PasswordBearer.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ftclient.PairlistApi();
apiInstance.whitelistApiV1WhitelistGet().then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters

This endpoint does not need any parameter.

### Return type

[**WhitelistResponse**](WhitelistResponse.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

