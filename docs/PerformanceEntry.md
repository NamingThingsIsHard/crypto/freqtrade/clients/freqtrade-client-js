# ftclient.PerformanceEntry

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pair** | **String** |  | 
**profit** | **Number** |  | 
**profitRatio** | **Number** |  | 
**profitPct** | **Number** |  | 
**profitAbs** | **Number** |  | 
**count** | **Number** |  | 


