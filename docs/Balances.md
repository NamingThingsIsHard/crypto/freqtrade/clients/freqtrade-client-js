# ftclient.Balances

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**currencies** | [**[Balance]**](Balance.md) |  | 
**total** | **Number** |  | 
**symbol** | **String** |  | 
**value** | **Number** |  | 
**stake** | **String** |  | 
**note** | **String** |  | 
**startingCapital** | **Number** |  | 
**startingCapitalRatio** | **Number** |  | 
**startingCapitalPct** | **Number** |  | 
**startingCapitalFiat** | **Number** |  | 
**startingCapitalFiatRatio** | **Number** |  | 
**startingCapitalFiatPct** | **Number** |  | 


