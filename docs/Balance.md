# ftclient.Balance

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**currency** | **String** |  | 
**free** | **Number** |  | 
**balance** | **Number** |  | 
**used** | **Number** |  | 
**estStake** | **Number** |  | 
**stake** | **String** |  | 


