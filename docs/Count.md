# ftclient.Count

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**current** | **Number** |  | 
**max** | **Number** |  | 
**totalStake** | **Number** |  | 


