# ftclient.Locks

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lockCount** | **Number** |  | 
**locks** | [**[LockModel]**](LockModel.md) |  | 


