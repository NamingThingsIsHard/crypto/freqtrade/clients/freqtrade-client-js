# ftclient.StrategyApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getStrategyApiV1StrategyStrategyGet**](StrategyApi.md#getStrategyApiV1StrategyStrategyGet) | **GET** /api/v1/strategy/{strategy} | Get Strategy
[**listStrategiesApiV1StrategiesGet**](StrategyApi.md#listStrategiesApiV1StrategiesGet) | **GET** /api/v1/strategies | List Strategies



## getStrategyApiV1StrategyStrategyGet

> StrategyResponse getStrategyApiV1StrategyStrategyGet(strategy)

Get Strategy

### Example

```javascript
import ftclient from 'freqtrade-client-js';
let defaultClient = ftclient.ApiClient.instance;
// Configure HTTP basic authorization: HTTPBasic
let HTTPBasic = defaultClient.authentications['HTTPBasic'];
HTTPBasic.username = 'YOUR USERNAME';
HTTPBasic.password = 'YOUR PASSWORD';
// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
let OAuth2PasswordBearer = defaultClient.authentications['OAuth2PasswordBearer'];
OAuth2PasswordBearer.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ftclient.StrategyApi();
let strategy = "strategy_example"; // String | 
apiInstance.getStrategyApiV1StrategyStrategyGet(strategy).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **strategy** | **String**|  | 

### Return type

[**StrategyResponse**](StrategyResponse.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## listStrategiesApiV1StrategiesGet

> StrategyListResponse listStrategiesApiV1StrategiesGet()

List Strategies

### Example

```javascript
import ftclient from 'freqtrade-client-js';
let defaultClient = ftclient.ApiClient.instance;
// Configure HTTP basic authorization: HTTPBasic
let HTTPBasic = defaultClient.authentications['HTTPBasic'];
HTTPBasic.username = 'YOUR USERNAME';
HTTPBasic.password = 'YOUR PASSWORD';
// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
let OAuth2PasswordBearer = defaultClient.authentications['OAuth2PasswordBearer'];
OAuth2PasswordBearer.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ftclient.StrategyApi();
apiInstance.listStrategiesApiV1StrategiesGet().then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters

This endpoint does not need any parameter.

### Return type

[**StrategyListResponse**](StrategyListResponse.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

