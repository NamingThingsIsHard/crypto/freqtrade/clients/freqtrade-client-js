# ftclient.InfoApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**balanceApiV1BalanceGet**](InfoApi.md#balanceApiV1BalanceGet) | **GET** /api/v1/balance | Balance
[**blacklistApiV1BlacklistGet**](InfoApi.md#blacklistApiV1BlacklistGet) | **GET** /api/v1/blacklist | Blacklist
[**blacklistDeleteApiV1BlacklistDelete**](InfoApi.md#blacklistDeleteApiV1BlacklistDelete) | **DELETE** /api/v1/blacklist | Blacklist Delete
[**blacklistPostApiV1BlacklistPost**](InfoApi.md#blacklistPostApiV1BlacklistPost) | **POST** /api/v1/blacklist | Blacklist Post
[**countApiV1CountGet**](InfoApi.md#countApiV1CountGet) | **GET** /api/v1/count | Count
[**dailyApiV1DailyGet**](InfoApi.md#dailyApiV1DailyGet) | **GET** /api/v1/daily | Daily
[**deleteLockApiV1LocksLockidDelete**](InfoApi.md#deleteLockApiV1LocksLockidDelete) | **DELETE** /api/v1/locks/{lockid} | Delete Lock
[**deleteLockPairApiV1LocksDeletePost**](InfoApi.md#deleteLockPairApiV1LocksDeletePost) | **POST** /api/v1/locks/delete | Delete Lock Pair
[**edgeApiV1EdgeGet**](InfoApi.md#edgeApiV1EdgeGet) | **GET** /api/v1/edge | Edge
[**locksApiV1LocksGet**](InfoApi.md#locksApiV1LocksGet) | **GET** /api/v1/locks | Locks
[**logsApiV1LogsGet**](InfoApi.md#logsApiV1LogsGet) | **GET** /api/v1/logs | Logs
[**performanceApiV1PerformanceGet**](InfoApi.md#performanceApiV1PerformanceGet) | **GET** /api/v1/performance | Performance
[**profitApiV1ProfitGet**](InfoApi.md#profitApiV1ProfitGet) | **GET** /api/v1/profit | Profit
[**showConfigApiV1ShowConfigGet**](InfoApi.md#showConfigApiV1ShowConfigGet) | **GET** /api/v1/show_config | Show Config
[**statsApiV1StatsGet**](InfoApi.md#statsApiV1StatsGet) | **GET** /api/v1/stats | Stats
[**statusApiV1StatusGet**](InfoApi.md#statusApiV1StatusGet) | **GET** /api/v1/status | Status
[**sysinfoApiV1SysinfoGet**](InfoApi.md#sysinfoApiV1SysinfoGet) | **GET** /api/v1/sysinfo | Sysinfo
[**tradeApiV1TradeTradeidGet**](InfoApi.md#tradeApiV1TradeTradeidGet) | **GET** /api/v1/trade/{tradeid} | Trade
[**tradesApiV1TradesGet**](InfoApi.md#tradesApiV1TradesGet) | **GET** /api/v1/trades | Trades
[**tradesDeleteApiV1TradesTradeidDelete**](InfoApi.md#tradesDeleteApiV1TradesTradeidDelete) | **DELETE** /api/v1/trades/{tradeid} | Trades Delete
[**versionApiV1VersionGet**](InfoApi.md#versionApiV1VersionGet) | **GET** /api/v1/version | Version
[**whitelistApiV1WhitelistGet**](InfoApi.md#whitelistApiV1WhitelistGet) | **GET** /api/v1/whitelist | Whitelist



## balanceApiV1BalanceGet

> Balances balanceApiV1BalanceGet()

Balance

Account Balances

### Example

```javascript
import ftclient from 'freqtrade-client-js';
let defaultClient = ftclient.ApiClient.instance;
// Configure HTTP basic authorization: HTTPBasic
let HTTPBasic = defaultClient.authentications['HTTPBasic'];
HTTPBasic.username = 'YOUR USERNAME';
HTTPBasic.password = 'YOUR PASSWORD';
// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
let OAuth2PasswordBearer = defaultClient.authentications['OAuth2PasswordBearer'];
OAuth2PasswordBearer.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ftclient.InfoApi();
apiInstance.balanceApiV1BalanceGet().then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters

This endpoint does not need any parameter.

### Return type

[**Balances**](Balances.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## blacklistApiV1BlacklistGet

> BlacklistResponse blacklistApiV1BlacklistGet()

Blacklist

### Example

```javascript
import ftclient from 'freqtrade-client-js';
let defaultClient = ftclient.ApiClient.instance;
// Configure HTTP basic authorization: HTTPBasic
let HTTPBasic = defaultClient.authentications['HTTPBasic'];
HTTPBasic.username = 'YOUR USERNAME';
HTTPBasic.password = 'YOUR PASSWORD';
// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
let OAuth2PasswordBearer = defaultClient.authentications['OAuth2PasswordBearer'];
OAuth2PasswordBearer.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ftclient.InfoApi();
apiInstance.blacklistApiV1BlacklistGet().then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters

This endpoint does not need any parameter.

### Return type

[**BlacklistResponse**](BlacklistResponse.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## blacklistDeleteApiV1BlacklistDelete

> BlacklistResponse blacklistDeleteApiV1BlacklistDelete(opts)

Blacklist Delete

Provide a list of pairs to delete from the blacklist

### Example

```javascript
import ftclient from 'freqtrade-client-js';
let defaultClient = ftclient.ApiClient.instance;
// Configure HTTP basic authorization: HTTPBasic
let HTTPBasic = defaultClient.authentications['HTTPBasic'];
HTTPBasic.username = 'YOUR USERNAME';
HTTPBasic.password = 'YOUR PASSWORD';
// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
let OAuth2PasswordBearer = defaultClient.authentications['OAuth2PasswordBearer'];
OAuth2PasswordBearer.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ftclient.InfoApi();
let opts = {
  'pairsToDelete': ["null"] // [String] | 
};
apiInstance.blacklistDeleteApiV1BlacklistDelete(opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pairsToDelete** | [**[String]**](String.md)|  | [optional] 

### Return type

[**BlacklistResponse**](BlacklistResponse.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## blacklistPostApiV1BlacklistPost

> BlacklistResponse blacklistPostApiV1BlacklistPost(blacklistPayload)

Blacklist Post

### Example

```javascript
import ftclient from 'freqtrade-client-js';
let defaultClient = ftclient.ApiClient.instance;
// Configure HTTP basic authorization: HTTPBasic
let HTTPBasic = defaultClient.authentications['HTTPBasic'];
HTTPBasic.username = 'YOUR USERNAME';
HTTPBasic.password = 'YOUR PASSWORD';
// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
let OAuth2PasswordBearer = defaultClient.authentications['OAuth2PasswordBearer'];
OAuth2PasswordBearer.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ftclient.InfoApi();
let blacklistPayload = new ftclient.BlacklistPayload(); // BlacklistPayload | 
apiInstance.blacklistPostApiV1BlacklistPost(blacklistPayload).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blacklistPayload** | [**BlacklistPayload**](BlacklistPayload.md)|  | 

### Return type

[**BlacklistResponse**](BlacklistResponse.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## countApiV1CountGet

> Count countApiV1CountGet()

Count

### Example

```javascript
import ftclient from 'freqtrade-client-js';
let defaultClient = ftclient.ApiClient.instance;
// Configure HTTP basic authorization: HTTPBasic
let HTTPBasic = defaultClient.authentications['HTTPBasic'];
HTTPBasic.username = 'YOUR USERNAME';
HTTPBasic.password = 'YOUR PASSWORD';
// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
let OAuth2PasswordBearer = defaultClient.authentications['OAuth2PasswordBearer'];
OAuth2PasswordBearer.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ftclient.InfoApi();
apiInstance.countApiV1CountGet().then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters

This endpoint does not need any parameter.

### Return type

[**Count**](Count.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## dailyApiV1DailyGet

> Daily dailyApiV1DailyGet(opts)

Daily

### Example

```javascript
import ftclient from 'freqtrade-client-js';
let defaultClient = ftclient.ApiClient.instance;
// Configure HTTP basic authorization: HTTPBasic
let HTTPBasic = defaultClient.authentications['HTTPBasic'];
HTTPBasic.username = 'YOUR USERNAME';
HTTPBasic.password = 'YOUR PASSWORD';
// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
let OAuth2PasswordBearer = defaultClient.authentications['OAuth2PasswordBearer'];
OAuth2PasswordBearer.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ftclient.InfoApi();
let opts = {
  'timescale': 7 // Number | 
};
apiInstance.dailyApiV1DailyGet(opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timescale** | **Number**|  | [optional] [default to 7]

### Return type

[**Daily**](Daily.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteLockApiV1LocksLockidDelete

> Locks deleteLockApiV1LocksLockidDelete(lockid)

Delete Lock

### Example

```javascript
import ftclient from 'freqtrade-client-js';
let defaultClient = ftclient.ApiClient.instance;
// Configure HTTP basic authorization: HTTPBasic
let HTTPBasic = defaultClient.authentications['HTTPBasic'];
HTTPBasic.username = 'YOUR USERNAME';
HTTPBasic.password = 'YOUR PASSWORD';
// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
let OAuth2PasswordBearer = defaultClient.authentications['OAuth2PasswordBearer'];
OAuth2PasswordBearer.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ftclient.InfoApi();
let lockid = 56; // Number | 
apiInstance.deleteLockApiV1LocksLockidDelete(lockid).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lockid** | **Number**|  | 

### Return type

[**Locks**](Locks.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteLockPairApiV1LocksDeletePost

> Locks deleteLockPairApiV1LocksDeletePost(deleteLockRequest)

Delete Lock Pair

### Example

```javascript
import ftclient from 'freqtrade-client-js';
let defaultClient = ftclient.ApiClient.instance;
// Configure HTTP basic authorization: HTTPBasic
let HTTPBasic = defaultClient.authentications['HTTPBasic'];
HTTPBasic.username = 'YOUR USERNAME';
HTTPBasic.password = 'YOUR PASSWORD';
// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
let OAuth2PasswordBearer = defaultClient.authentications['OAuth2PasswordBearer'];
OAuth2PasswordBearer.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ftclient.InfoApi();
let deleteLockRequest = new ftclient.DeleteLockRequest(); // DeleteLockRequest | 
apiInstance.deleteLockPairApiV1LocksDeletePost(deleteLockRequest).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deleteLockRequest** | [**DeleteLockRequest**](DeleteLockRequest.md)|  | 

### Return type

[**Locks**](Locks.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## edgeApiV1EdgeGet

> Object edgeApiV1EdgeGet()

Edge

### Example

```javascript
import ftclient from 'freqtrade-client-js';
let defaultClient = ftclient.ApiClient.instance;
// Configure HTTP basic authorization: HTTPBasic
let HTTPBasic = defaultClient.authentications['HTTPBasic'];
HTTPBasic.username = 'YOUR USERNAME';
HTTPBasic.password = 'YOUR PASSWORD';
// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
let OAuth2PasswordBearer = defaultClient.authentications['OAuth2PasswordBearer'];
OAuth2PasswordBearer.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ftclient.InfoApi();
apiInstance.edgeApiV1EdgeGet().then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters

This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## locksApiV1LocksGet

> Locks locksApiV1LocksGet()

Locks

### Example

```javascript
import ftclient from 'freqtrade-client-js';
let defaultClient = ftclient.ApiClient.instance;
// Configure HTTP basic authorization: HTTPBasic
let HTTPBasic = defaultClient.authentications['HTTPBasic'];
HTTPBasic.username = 'YOUR USERNAME';
HTTPBasic.password = 'YOUR PASSWORD';
// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
let OAuth2PasswordBearer = defaultClient.authentications['OAuth2PasswordBearer'];
OAuth2PasswordBearer.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ftclient.InfoApi();
apiInstance.locksApiV1LocksGet().then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters

This endpoint does not need any parameter.

### Return type

[**Locks**](Locks.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## logsApiV1LogsGet

> Logs logsApiV1LogsGet(opts)

Logs

### Example

```javascript
import ftclient from 'freqtrade-client-js';
let defaultClient = ftclient.ApiClient.instance;
// Configure HTTP basic authorization: HTTPBasic
let HTTPBasic = defaultClient.authentications['HTTPBasic'];
HTTPBasic.username = 'YOUR USERNAME';
HTTPBasic.password = 'YOUR PASSWORD';
// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
let OAuth2PasswordBearer = defaultClient.authentications['OAuth2PasswordBearer'];
OAuth2PasswordBearer.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ftclient.InfoApi();
let opts = {
  'limit': 56 // Number | 
};
apiInstance.logsApiV1LogsGet(opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **Number**|  | [optional] 

### Return type

[**Logs**](Logs.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## performanceApiV1PerformanceGet

> [PerformanceEntry] performanceApiV1PerformanceGet()

Performance

### Example

```javascript
import ftclient from 'freqtrade-client-js';
let defaultClient = ftclient.ApiClient.instance;
// Configure HTTP basic authorization: HTTPBasic
let HTTPBasic = defaultClient.authentications['HTTPBasic'];
HTTPBasic.username = 'YOUR USERNAME';
HTTPBasic.password = 'YOUR PASSWORD';
// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
let OAuth2PasswordBearer = defaultClient.authentications['OAuth2PasswordBearer'];
OAuth2PasswordBearer.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ftclient.InfoApi();
apiInstance.performanceApiV1PerformanceGet().then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters

This endpoint does not need any parameter.

### Return type

[**[PerformanceEntry]**](PerformanceEntry.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## profitApiV1ProfitGet

> Profit profitApiV1ProfitGet()

Profit

### Example

```javascript
import ftclient from 'freqtrade-client-js';
let defaultClient = ftclient.ApiClient.instance;
// Configure HTTP basic authorization: HTTPBasic
let HTTPBasic = defaultClient.authentications['HTTPBasic'];
HTTPBasic.username = 'YOUR USERNAME';
HTTPBasic.password = 'YOUR PASSWORD';
// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
let OAuth2PasswordBearer = defaultClient.authentications['OAuth2PasswordBearer'];
OAuth2PasswordBearer.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ftclient.InfoApi();
apiInstance.profitApiV1ProfitGet().then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters

This endpoint does not need any parameter.

### Return type

[**Profit**](Profit.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## showConfigApiV1ShowConfigGet

> ShowConfig showConfigApiV1ShowConfigGet()

Show Config

### Example

```javascript
import ftclient from 'freqtrade-client-js';
let defaultClient = ftclient.ApiClient.instance;
// Configure HTTP basic authorization: HTTPBasic
let HTTPBasic = defaultClient.authentications['HTTPBasic'];
HTTPBasic.username = 'YOUR USERNAME';
HTTPBasic.password = 'YOUR PASSWORD';
// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
let OAuth2PasswordBearer = defaultClient.authentications['OAuth2PasswordBearer'];
OAuth2PasswordBearer.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ftclient.InfoApi();
apiInstance.showConfigApiV1ShowConfigGet().then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters

This endpoint does not need any parameter.

### Return type

[**ShowConfig**](ShowConfig.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## statsApiV1StatsGet

> Stats statsApiV1StatsGet()

Stats

### Example

```javascript
import ftclient from 'freqtrade-client-js';
let defaultClient = ftclient.ApiClient.instance;
// Configure HTTP basic authorization: HTTPBasic
let HTTPBasic = defaultClient.authentications['HTTPBasic'];
HTTPBasic.username = 'YOUR USERNAME';
HTTPBasic.password = 'YOUR PASSWORD';
// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
let OAuth2PasswordBearer = defaultClient.authentications['OAuth2PasswordBearer'];
OAuth2PasswordBearer.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ftclient.InfoApi();
apiInstance.statsApiV1StatsGet().then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters

This endpoint does not need any parameter.

### Return type

[**Stats**](Stats.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## statusApiV1StatusGet

> [OpenTradeSchema] statusApiV1StatusGet()

Status

### Example

```javascript
import ftclient from 'freqtrade-client-js';
let defaultClient = ftclient.ApiClient.instance;
// Configure HTTP basic authorization: HTTPBasic
let HTTPBasic = defaultClient.authentications['HTTPBasic'];
HTTPBasic.username = 'YOUR USERNAME';
HTTPBasic.password = 'YOUR PASSWORD';
// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
let OAuth2PasswordBearer = defaultClient.authentications['OAuth2PasswordBearer'];
OAuth2PasswordBearer.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ftclient.InfoApi();
apiInstance.statusApiV1StatusGet().then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters

This endpoint does not need any parameter.

### Return type

[**[OpenTradeSchema]**](OpenTradeSchema.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## sysinfoApiV1SysinfoGet

> SysInfo sysinfoApiV1SysinfoGet()

Sysinfo

### Example

```javascript
import ftclient from 'freqtrade-client-js';
let defaultClient = ftclient.ApiClient.instance;
// Configure HTTP basic authorization: HTTPBasic
let HTTPBasic = defaultClient.authentications['HTTPBasic'];
HTTPBasic.username = 'YOUR USERNAME';
HTTPBasic.password = 'YOUR PASSWORD';
// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
let OAuth2PasswordBearer = defaultClient.authentications['OAuth2PasswordBearer'];
OAuth2PasswordBearer.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ftclient.InfoApi();
apiInstance.sysinfoApiV1SysinfoGet().then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters

This endpoint does not need any parameter.

### Return type

[**SysInfo**](SysInfo.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## tradeApiV1TradeTradeidGet

> OpenTradeSchema tradeApiV1TradeTradeidGet(tradeid)

Trade

### Example

```javascript
import ftclient from 'freqtrade-client-js';
let defaultClient = ftclient.ApiClient.instance;
// Configure HTTP basic authorization: HTTPBasic
let HTTPBasic = defaultClient.authentications['HTTPBasic'];
HTTPBasic.username = 'YOUR USERNAME';
HTTPBasic.password = 'YOUR PASSWORD';
// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
let OAuth2PasswordBearer = defaultClient.authentications['OAuth2PasswordBearer'];
OAuth2PasswordBearer.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ftclient.InfoApi();
let tradeid = 56; // Number | 
apiInstance.tradeApiV1TradeTradeidGet(tradeid).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tradeid** | **Number**|  | 

### Return type

[**OpenTradeSchema**](OpenTradeSchema.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## tradesApiV1TradesGet

> Object tradesApiV1TradesGet(opts)

Trades

### Example

```javascript
import ftclient from 'freqtrade-client-js';
let defaultClient = ftclient.ApiClient.instance;
// Configure HTTP basic authorization: HTTPBasic
let HTTPBasic = defaultClient.authentications['HTTPBasic'];
HTTPBasic.username = 'YOUR USERNAME';
HTTPBasic.password = 'YOUR PASSWORD';
// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
let OAuth2PasswordBearer = defaultClient.authentications['OAuth2PasswordBearer'];
OAuth2PasswordBearer.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ftclient.InfoApi();
let opts = {
  'limit': 500, // Number | 
  'offset': 0 // Number | 
};
apiInstance.tradesApiV1TradesGet(opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **Number**|  | [optional] [default to 500]
 **offset** | **Number**|  | [optional] [default to 0]

### Return type

**Object**

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## tradesDeleteApiV1TradesTradeidDelete

> DeleteTrade tradesDeleteApiV1TradesTradeidDelete(tradeid)

Trades Delete

### Example

```javascript
import ftclient from 'freqtrade-client-js';
let defaultClient = ftclient.ApiClient.instance;
// Configure HTTP basic authorization: HTTPBasic
let HTTPBasic = defaultClient.authentications['HTTPBasic'];
HTTPBasic.username = 'YOUR USERNAME';
HTTPBasic.password = 'YOUR PASSWORD';
// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
let OAuth2PasswordBearer = defaultClient.authentications['OAuth2PasswordBearer'];
OAuth2PasswordBearer.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ftclient.InfoApi();
let tradeid = 56; // Number | 
apiInstance.tradesDeleteApiV1TradesTradeidDelete(tradeid).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tradeid** | **Number**|  | 

### Return type

[**DeleteTrade**](DeleteTrade.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## versionApiV1VersionGet

> Version versionApiV1VersionGet()

Version

Bot Version info

### Example

```javascript
import ftclient from 'freqtrade-client-js';
let defaultClient = ftclient.ApiClient.instance;
// Configure HTTP basic authorization: HTTPBasic
let HTTPBasic = defaultClient.authentications['HTTPBasic'];
HTTPBasic.username = 'YOUR USERNAME';
HTTPBasic.password = 'YOUR PASSWORD';
// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
let OAuth2PasswordBearer = defaultClient.authentications['OAuth2PasswordBearer'];
OAuth2PasswordBearer.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ftclient.InfoApi();
apiInstance.versionApiV1VersionGet().then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters

This endpoint does not need any parameter.

### Return type

[**Version**](Version.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## whitelistApiV1WhitelistGet

> WhitelistResponse whitelistApiV1WhitelistGet()

Whitelist

### Example

```javascript
import ftclient from 'freqtrade-client-js';
let defaultClient = ftclient.ApiClient.instance;
// Configure HTTP basic authorization: HTTPBasic
let HTTPBasic = defaultClient.authentications['HTTPBasic'];
HTTPBasic.username = 'YOUR USERNAME';
HTTPBasic.password = 'YOUR PASSWORD';
// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
let OAuth2PasswordBearer = defaultClient.authentications['OAuth2PasswordBearer'];
OAuth2PasswordBearer.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ftclient.InfoApi();
apiInstance.whitelistApiV1WhitelistGet().then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters

This endpoint does not need any parameter.

### Return type

[**WhitelistResponse**](WhitelistResponse.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

