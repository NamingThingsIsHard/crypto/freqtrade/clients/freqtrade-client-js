# ftclient.BacktestApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiBacktestAbortApiV1BacktestAbortGet**](BacktestApi.md#apiBacktestAbortApiV1BacktestAbortGet) | **GET** /api/v1/backtest/abort | Api Backtest Abort
[**apiDeleteBacktestApiV1BacktestDelete**](BacktestApi.md#apiDeleteBacktestApiV1BacktestDelete) | **DELETE** /api/v1/backtest | Api Delete Backtest
[**apiGetBacktestApiV1BacktestGet**](BacktestApi.md#apiGetBacktestApiV1BacktestGet) | **GET** /api/v1/backtest | Api Get Backtest
[**apiStartBacktestApiV1BacktestPost**](BacktestApi.md#apiStartBacktestApiV1BacktestPost) | **POST** /api/v1/backtest | Api Start Backtest



## apiBacktestAbortApiV1BacktestAbortGet

> BacktestResponse apiBacktestAbortApiV1BacktestAbortGet()

Api Backtest Abort

### Example

```javascript
import ftclient from 'freqtrade-client-js';
let defaultClient = ftclient.ApiClient.instance;
// Configure HTTP basic authorization: HTTPBasic
let HTTPBasic = defaultClient.authentications['HTTPBasic'];
HTTPBasic.username = 'YOUR USERNAME';
HTTPBasic.password = 'YOUR PASSWORD';
// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
let OAuth2PasswordBearer = defaultClient.authentications['OAuth2PasswordBearer'];
OAuth2PasswordBearer.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ftclient.BacktestApi();
apiInstance.apiBacktestAbortApiV1BacktestAbortGet().then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters

This endpoint does not need any parameter.

### Return type

[**BacktestResponse**](BacktestResponse.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## apiDeleteBacktestApiV1BacktestDelete

> BacktestResponse apiDeleteBacktestApiV1BacktestDelete()

Api Delete Backtest

Reset backtesting

### Example

```javascript
import ftclient from 'freqtrade-client-js';
let defaultClient = ftclient.ApiClient.instance;
// Configure HTTP basic authorization: HTTPBasic
let HTTPBasic = defaultClient.authentications['HTTPBasic'];
HTTPBasic.username = 'YOUR USERNAME';
HTTPBasic.password = 'YOUR PASSWORD';
// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
let OAuth2PasswordBearer = defaultClient.authentications['OAuth2PasswordBearer'];
OAuth2PasswordBearer.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ftclient.BacktestApi();
apiInstance.apiDeleteBacktestApiV1BacktestDelete().then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters

This endpoint does not need any parameter.

### Return type

[**BacktestResponse**](BacktestResponse.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## apiGetBacktestApiV1BacktestGet

> BacktestResponse apiGetBacktestApiV1BacktestGet()

Api Get Backtest

Get backtesting result. Returns Result after backtesting has been ran.

### Example

```javascript
import ftclient from 'freqtrade-client-js';
let defaultClient = ftclient.ApiClient.instance;
// Configure HTTP basic authorization: HTTPBasic
let HTTPBasic = defaultClient.authentications['HTTPBasic'];
HTTPBasic.username = 'YOUR USERNAME';
HTTPBasic.password = 'YOUR PASSWORD';
// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
let OAuth2PasswordBearer = defaultClient.authentications['OAuth2PasswordBearer'];
OAuth2PasswordBearer.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ftclient.BacktestApi();
apiInstance.apiGetBacktestApiV1BacktestGet().then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters

This endpoint does not need any parameter.

### Return type

[**BacktestResponse**](BacktestResponse.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## apiStartBacktestApiV1BacktestPost

> BacktestResponse apiStartBacktestApiV1BacktestPost(backtestRequest)

Api Start Backtest

Start backtesting if not done so already

### Example

```javascript
import ftclient from 'freqtrade-client-js';
let defaultClient = ftclient.ApiClient.instance;
// Configure HTTP basic authorization: HTTPBasic
let HTTPBasic = defaultClient.authentications['HTTPBasic'];
HTTPBasic.username = 'YOUR USERNAME';
HTTPBasic.password = 'YOUR PASSWORD';
// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
let OAuth2PasswordBearer = defaultClient.authentications['OAuth2PasswordBearer'];
OAuth2PasswordBearer.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ftclient.BacktestApi();
let backtestRequest = new ftclient.BacktestRequest(); // BacktestRequest | 
apiInstance.apiStartBacktestApiV1BacktestPost(backtestRequest).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **backtestRequest** | [**BacktestRequest**](BacktestRequest.md)|  | 

### Return type

[**BacktestResponse**](BacktestResponse.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

