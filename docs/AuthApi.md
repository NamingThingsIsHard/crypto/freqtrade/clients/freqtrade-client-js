# ftclient.AuthApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**tokenLoginApiV1TokenLoginPost**](AuthApi.md#tokenLoginApiV1TokenLoginPost) | **POST** /api/v1/token/login | Token Login
[**tokenRefreshApiV1TokenRefreshPost**](AuthApi.md#tokenRefreshApiV1TokenRefreshPost) | **POST** /api/v1/token/refresh | Token Refresh



## tokenLoginApiV1TokenLoginPost

> AccessAndRefreshToken tokenLoginApiV1TokenLoginPost()

Token Login

### Example

```javascript
import ftclient from 'freqtrade-client-js';
let defaultClient = ftclient.ApiClient.instance;
// Configure HTTP basic authorization: HTTPBasic
let HTTPBasic = defaultClient.authentications['HTTPBasic'];
HTTPBasic.username = 'YOUR USERNAME';
HTTPBasic.password = 'YOUR PASSWORD';

let apiInstance = new ftclient.AuthApi();
apiInstance.tokenLoginApiV1TokenLoginPost().then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters

This endpoint does not need any parameter.

### Return type

[**AccessAndRefreshToken**](AccessAndRefreshToken.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## tokenRefreshApiV1TokenRefreshPost

> AccessToken tokenRefreshApiV1TokenRefreshPost()

Token Refresh

### Example

```javascript
import ftclient from 'freqtrade-client-js';
let defaultClient = ftclient.ApiClient.instance;
// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
let OAuth2PasswordBearer = defaultClient.authentications['OAuth2PasswordBearer'];
OAuth2PasswordBearer.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ftclient.AuthApi();
apiInstance.tokenRefreshApiV1TokenRefreshPost().then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters

This endpoint does not need any parameter.

### Return type

[**AccessToken**](AccessToken.md)

### Authorization

[OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

