# ftclient.AccessAndRefreshToken

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accessToken** | **String** |  | 
**refreshToken** | **String** |  | 


