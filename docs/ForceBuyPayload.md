# ftclient.ForceBuyPayload

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pair** | **String** |  | 
**price** | **Number** |  | [optional] 
**ordertype** | [**OrderTypeValues**](OrderTypeValues.md) |  | [optional] 


