# ftclient.BacktestResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **String** |  | 
**running** | **Boolean** |  | 
**statusMsg** | **String** |  | 
**step** | **String** |  | 
**progress** | **Number** |  | 
**tradeCount** | **Number** |  | [optional] 
**backtestResult** | **Object** |  | [optional] 


