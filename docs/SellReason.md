# ftclient.SellReason

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**wins** | **Number** |  | 
**losses** | **Number** |  | 
**draws** | **Number** |  | 


