# ftclient.SysInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cpuPct** | **[Number]** |  | 
**ramPct** | **Number** |  | 


