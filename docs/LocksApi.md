# ftclient.LocksApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteLockApiV1LocksLockidDelete**](LocksApi.md#deleteLockApiV1LocksLockidDelete) | **DELETE** /api/v1/locks/{lockid} | Delete Lock
[**deleteLockPairApiV1LocksDeletePost**](LocksApi.md#deleteLockPairApiV1LocksDeletePost) | **POST** /api/v1/locks/delete | Delete Lock Pair
[**locksApiV1LocksGet**](LocksApi.md#locksApiV1LocksGet) | **GET** /api/v1/locks | Locks



## deleteLockApiV1LocksLockidDelete

> Locks deleteLockApiV1LocksLockidDelete(lockid)

Delete Lock

### Example

```javascript
import ftclient from 'freqtrade-client-js';
let defaultClient = ftclient.ApiClient.instance;
// Configure HTTP basic authorization: HTTPBasic
let HTTPBasic = defaultClient.authentications['HTTPBasic'];
HTTPBasic.username = 'YOUR USERNAME';
HTTPBasic.password = 'YOUR PASSWORD';
// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
let OAuth2PasswordBearer = defaultClient.authentications['OAuth2PasswordBearer'];
OAuth2PasswordBearer.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ftclient.LocksApi();
let lockid = 56; // Number | 
apiInstance.deleteLockApiV1LocksLockidDelete(lockid).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lockid** | **Number**|  | 

### Return type

[**Locks**](Locks.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteLockPairApiV1LocksDeletePost

> Locks deleteLockPairApiV1LocksDeletePost(deleteLockRequest)

Delete Lock Pair

### Example

```javascript
import ftclient from 'freqtrade-client-js';
let defaultClient = ftclient.ApiClient.instance;
// Configure HTTP basic authorization: HTTPBasic
let HTTPBasic = defaultClient.authentications['HTTPBasic'];
HTTPBasic.username = 'YOUR USERNAME';
HTTPBasic.password = 'YOUR PASSWORD';
// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
let OAuth2PasswordBearer = defaultClient.authentications['OAuth2PasswordBearer'];
OAuth2PasswordBearer.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ftclient.LocksApi();
let deleteLockRequest = new ftclient.DeleteLockRequest(); // DeleteLockRequest | 
apiInstance.deleteLockPairApiV1LocksDeletePost(deleteLockRequest).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deleteLockRequest** | [**DeleteLockRequest**](DeleteLockRequest.md)|  | 

### Return type

[**Locks**](Locks.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## locksApiV1LocksGet

> Locks locksApiV1LocksGet()

Locks

### Example

```javascript
import ftclient from 'freqtrade-client-js';
let defaultClient = ftclient.ApiClient.instance;
// Configure HTTP basic authorization: HTTPBasic
let HTTPBasic = defaultClient.authentications['HTTPBasic'];
HTTPBasic.username = 'YOUR USERNAME';
HTTPBasic.password = 'YOUR PASSWORD';
// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
let OAuth2PasswordBearer = defaultClient.authentications['OAuth2PasswordBearer'];
OAuth2PasswordBearer.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ftclient.LocksApi();
apiInstance.locksApiV1LocksGet().then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters

This endpoint does not need any parameter.

### Return type

[**Locks**](Locks.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

