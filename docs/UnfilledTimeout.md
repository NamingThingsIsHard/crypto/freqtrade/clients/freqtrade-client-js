# ftclient.UnfilledTimeout

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**buy** | **Number** |  | [optional] 
**sell** | **Number** |  | [optional] 
**unit** | **String** |  | [optional] 
**exitTimeoutCount** | **Number** |  | [optional] 


