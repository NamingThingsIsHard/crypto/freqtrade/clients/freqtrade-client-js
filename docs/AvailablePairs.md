# ftclient.AvailablePairs

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**length** | **Number** |  | 
**pairs** | **[String]** |  | 
**pairInterval** | **[[String]]** |  | 


