# ftclient.DailyRecord

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date** | **Date** |  | 
**absProfit** | **Number** |  | 
**fiatValue** | **Number** |  | 
**tradeCount** | **Number** |  | 


