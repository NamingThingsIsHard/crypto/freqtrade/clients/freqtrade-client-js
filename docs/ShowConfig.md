# ftclient.ShowConfig

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**version** | **String** |  | 
**strategyVersion** | **String** |  | [optional] 
**apiVersion** | **Number** |  | 
**dryRun** | **Boolean** |  | 
**stakeCurrency** | **String** |  | 
**stakeAmount** | [**AnyOfnumberstring**](AnyOfnumberstring.md) |  | 
**availableCapital** | **Number** |  | [optional] 
**stakeCurrencyDecimals** | **Number** |  | 
**maxOpenTrades** | **Number** |  | 
**minimalRoi** | **Object** |  | 
**stoploss** | **Number** |  | [optional] 
**trailingStop** | **Boolean** |  | [optional] 
**trailingStopPositive** | **Number** |  | [optional] 
**trailingStopPositiveOffset** | **Number** |  | [optional] 
**trailingOnlyOffsetIsReached** | **Boolean** |  | [optional] 
**unfilledtimeout** | [**UnfilledTimeout**](UnfilledTimeout.md) |  | 
**orderTypes** | [**OrderTypes**](OrderTypes.md) |  | [optional] 
**useCustomStoploss** | **Boolean** |  | [optional] 
**timeframe** | **String** |  | [optional] 
**timeframeMs** | **Number** |  | 
**timeframeMin** | **Number** |  | 
**exchange** | **String** |  | 
**strategy** | **String** |  | [optional] 
**forcebuyEnabled** | **Boolean** |  | 
**askStrategy** | **Object** |  | 
**bidStrategy** | **Object** |  | 
**botName** | **String** |  | 
**state** | **String** |  | 
**runmode** | **String** |  | 


