# ftclient.CandleDataApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**listAvailablePairsApiV1AvailablePairsGet**](CandleDataApi.md#listAvailablePairsApiV1AvailablePairsGet) | **GET** /api/v1/available_pairs | List Available Pairs
[**pairCandlesApiV1PairCandlesGet**](CandleDataApi.md#pairCandlesApiV1PairCandlesGet) | **GET** /api/v1/pair_candles | Pair Candles
[**pairHistoryApiV1PairHistoryGet**](CandleDataApi.md#pairHistoryApiV1PairHistoryGet) | **GET** /api/v1/pair_history | Pair History
[**plotConfigApiV1PlotConfigGet**](CandleDataApi.md#plotConfigApiV1PlotConfigGet) | **GET** /api/v1/plot_config | Plot Config



## listAvailablePairsApiV1AvailablePairsGet

> AvailablePairs listAvailablePairsApiV1AvailablePairsGet(opts)

List Available Pairs

### Example

```javascript
import ftclient from 'freqtrade-client-js';
let defaultClient = ftclient.ApiClient.instance;
// Configure HTTP basic authorization: HTTPBasic
let HTTPBasic = defaultClient.authentications['HTTPBasic'];
HTTPBasic.username = 'YOUR USERNAME';
HTTPBasic.password = 'YOUR PASSWORD';
// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
let OAuth2PasswordBearer = defaultClient.authentications['OAuth2PasswordBearer'];
OAuth2PasswordBearer.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ftclient.CandleDataApi();
let opts = {
  'timeframe': "timeframe_example", // String | 
  'stakeCurrency': "stakeCurrency_example" // String | 
};
apiInstance.listAvailablePairsApiV1AvailablePairsGet(opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeframe** | **String**|  | [optional] 
 **stakeCurrency** | **String**|  | [optional] 

### Return type

[**AvailablePairs**](AvailablePairs.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## pairCandlesApiV1PairCandlesGet

> PairHistory pairCandlesApiV1PairCandlesGet(pair, timeframe, limit)

Pair Candles

### Example

```javascript
import ftclient from 'freqtrade-client-js';
let defaultClient = ftclient.ApiClient.instance;
// Configure HTTP basic authorization: HTTPBasic
let HTTPBasic = defaultClient.authentications['HTTPBasic'];
HTTPBasic.username = 'YOUR USERNAME';
HTTPBasic.password = 'YOUR PASSWORD';
// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
let OAuth2PasswordBearer = defaultClient.authentications['OAuth2PasswordBearer'];
OAuth2PasswordBearer.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ftclient.CandleDataApi();
let pair = "pair_example"; // String | 
let timeframe = "timeframe_example"; // String | 
let limit = 56; // Number | 
apiInstance.pairCandlesApiV1PairCandlesGet(pair, timeframe, limit).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pair** | **String**|  | 
 **timeframe** | **String**|  | 
 **limit** | **Number**|  | 

### Return type

[**PairHistory**](PairHistory.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## pairHistoryApiV1PairHistoryGet

> PairHistory pairHistoryApiV1PairHistoryGet(pair, timeframe, timerange, strategy)

Pair History

### Example

```javascript
import ftclient from 'freqtrade-client-js';
let defaultClient = ftclient.ApiClient.instance;
// Configure HTTP basic authorization: HTTPBasic
let HTTPBasic = defaultClient.authentications['HTTPBasic'];
HTTPBasic.username = 'YOUR USERNAME';
HTTPBasic.password = 'YOUR PASSWORD';
// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
let OAuth2PasswordBearer = defaultClient.authentications['OAuth2PasswordBearer'];
OAuth2PasswordBearer.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ftclient.CandleDataApi();
let pair = "pair_example"; // String | 
let timeframe = "timeframe_example"; // String | 
let timerange = "timerange_example"; // String | 
let strategy = "strategy_example"; // String | 
apiInstance.pairHistoryApiV1PairHistoryGet(pair, timeframe, timerange, strategy).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pair** | **String**|  | 
 **timeframe** | **String**|  | 
 **timerange** | **String**|  | 
 **strategy** | **String**|  | 

### Return type

[**PairHistory**](PairHistory.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## plotConfigApiV1PlotConfigGet

> PlotConfig plotConfigApiV1PlotConfigGet()

Plot Config

### Example

```javascript
import ftclient from 'freqtrade-client-js';
let defaultClient = ftclient.ApiClient.instance;
// Configure HTTP basic authorization: HTTPBasic
let HTTPBasic = defaultClient.authentications['HTTPBasic'];
HTTPBasic.username = 'YOUR USERNAME';
HTTPBasic.password = 'YOUR PASSWORD';
// Configure OAuth2 access token for authorization: OAuth2PasswordBearer
let OAuth2PasswordBearer = defaultClient.authentications['OAuth2PasswordBearer'];
OAuth2PasswordBearer.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ftclient.CandleDataApi();
apiInstance.plotConfigApiV1PlotConfigGet().then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters

This endpoint does not need any parameter.

### Return type

[**PlotConfig**](PlotConfig.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

