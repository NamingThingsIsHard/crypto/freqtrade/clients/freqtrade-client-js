# ftclient.Profit

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**profitClosedCoin** | **Number** |  | 
**profitClosedPercentMean** | **Number** |  | 
**profitClosedRatioMean** | **Number** |  | 
**profitClosedPercentSum** | **Number** |  | 
**profitClosedRatioSum** | **Number** |  | 
**profitClosedPercent** | **Number** |  | 
**profitClosedRatio** | **Number** |  | 
**profitClosedFiat** | **Number** |  | 
**profitAllCoin** | **Number** |  | 
**profitAllPercentMean** | **Number** |  | 
**profitAllRatioMean** | **Number** |  | 
**profitAllPercentSum** | **Number** |  | 
**profitAllRatioSum** | **Number** |  | 
**profitAllPercent** | **Number** |  | 
**profitAllRatio** | **Number** |  | 
**profitAllFiat** | **Number** |  | 
**tradeCount** | **Number** |  | 
**closedTradeCount** | **Number** |  | 
**firstTradeDate** | **String** |  | 
**firstTradeTimestamp** | **Number** |  | 
**latestTradeDate** | **String** |  | 
**latestTradeTimestamp** | **Number** |  | 
**avgDuration** | **String** |  | 
**bestPair** | **String** |  | 
**bestRate** | **Number** |  | 
**bestPairProfitRatio** | **Number** |  | 
**winningTrades** | **Number** |  | 
**losingTrades** | **Number** |  | 


