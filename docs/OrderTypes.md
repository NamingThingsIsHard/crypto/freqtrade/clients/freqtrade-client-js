# ftclient.OrderTypes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**buy** | [**OrderTypeValues**](OrderTypeValues.md) |  | 
**sell** | [**OrderTypeValues**](OrderTypeValues.md) |  | 
**emergencysell** | [**OrderTypeValues**](OrderTypeValues.md) |  | [optional] 
**forcesell** | [**OrderTypeValues**](OrderTypeValues.md) |  | [optional] 
**forcebuy** | [**OrderTypeValues**](OrderTypeValues.md) |  | [optional] 
**stoploss** | [**OrderTypeValues**](OrderTypeValues.md) |  | 
**stoplossOnExchange** | **Boolean** |  | 
**stoplossOnExchangeInterval** | **Number** |  | [optional] 


