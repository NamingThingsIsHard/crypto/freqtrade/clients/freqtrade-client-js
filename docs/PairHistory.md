# ftclient.PairHistory

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**strategy** | **String** |  | 
**pair** | **String** |  | 
**timeframe** | **String** |  | 
**timeframeMs** | **Number** |  | 
**columns** | **[String]** |  | 
**data** | **[Object]** |  | 
**length** | **Number** |  | 
**buySignals** | **Number** |  | 
**sellSignals** | **Number** |  | 
**lastAnalyzed** | **Date** |  | 
**lastAnalyzedTs** | **Number** |  | 
**dataStartTs** | **Number** |  | 
**dataStart** | **String** |  | 
**dataStop** | **String** |  | 
**dataStopTs** | **Number** |  | 


