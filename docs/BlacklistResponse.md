# ftclient.BlacklistResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**blacklist** | **[String]** |  | 
**blacklistExpanded** | **[String]** |  | 
**errors** | **Object** |  | 
**length** | **Number** |  | 
**method** | **[String]** |  | 


