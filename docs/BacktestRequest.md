# ftclient.BacktestRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**strategy** | **String** |  | 
**timeframe** | **String** |  | [optional] 
**timeframeDetail** | **String** |  | [optional] 
**timerange** | **String** |  | [optional] 
**maxOpenTrades** | **Number** |  | [optional] 
**stakeAmount** | [**AnyOfnumberstring**](AnyOfnumberstring.md) |  | [optional] 
**enableProtections** | **Boolean** |  | 
**dryRunWallet** | **Number** |  | [optional] 


