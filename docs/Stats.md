# ftclient.Stats

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sellReasons** | [**{String: SellReason}**](SellReason.md) |  | 
**durations** | [**{String: AnyOfstringnumber}**](AnyOfstringnumber.md) |  | 


