# ftclient.DeleteLockRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pair** | **String** |  | [optional] 
**lockid** | **Number** |  | [optional] 


