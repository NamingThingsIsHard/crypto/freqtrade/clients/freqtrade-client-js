/**
 * Freqtrade API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 2021.12-dev21
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import OrderTypeValues from './OrderTypeValues';

/**
 * The ForceBuyPayload model module.
 * @module model/ForceBuyPayload
 * @version 2021.12-dev21
 */
class ForceBuyPayload {
    /**
     * Constructs a new <code>ForceBuyPayload</code>.
     * @alias module:model/ForceBuyPayload
     * @param pair {String} 
     */
    constructor(pair) { 
        
        ForceBuyPayload.initialize(this, pair);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, pair) { 
        obj['pair'] = pair;
    }

    /**
     * Constructs a <code>ForceBuyPayload</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/ForceBuyPayload} obj Optional instance to populate.
     * @return {module:model/ForceBuyPayload} The populated <code>ForceBuyPayload</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new ForceBuyPayload();

            if (data.hasOwnProperty('pair')) {
                obj['pair'] = ApiClient.convertToType(data['pair'], 'String');
            }
            if (data.hasOwnProperty('price')) {
                obj['price'] = ApiClient.convertToType(data['price'], 'Number');
            }
            if (data.hasOwnProperty('ordertype')) {
                obj['ordertype'] = OrderTypeValues.constructFromObject(data['ordertype']);
            }
        }
        return obj;
    }


}

/**
 * @member {String} pair
 */
ForceBuyPayload.prototype['pair'] = undefined;

/**
 * @member {Number} price
 */
ForceBuyPayload.prototype['price'] = undefined;

/**
 * @member {module:model/OrderTypeValues} ordertype
 */
ForceBuyPayload.prototype['ordertype'] = undefined;






export default ForceBuyPayload;

