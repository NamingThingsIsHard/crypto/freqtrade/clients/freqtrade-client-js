/**
 * Freqtrade API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 2021.12-dev21
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The UnfilledTimeout model module.
 * @module model/UnfilledTimeout
 * @version 2021.12-dev21
 */
class UnfilledTimeout {
    /**
     * Constructs a new <code>UnfilledTimeout</code>.
     * @alias module:model/UnfilledTimeout
     */
    constructor() { 
        
        UnfilledTimeout.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>UnfilledTimeout</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/UnfilledTimeout} obj Optional instance to populate.
     * @return {module:model/UnfilledTimeout} The populated <code>UnfilledTimeout</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new UnfilledTimeout();

            if (data.hasOwnProperty('buy')) {
                obj['buy'] = ApiClient.convertToType(data['buy'], 'Number');
            }
            if (data.hasOwnProperty('sell')) {
                obj['sell'] = ApiClient.convertToType(data['sell'], 'Number');
            }
            if (data.hasOwnProperty('unit')) {
                obj['unit'] = ApiClient.convertToType(data['unit'], 'String');
            }
            if (data.hasOwnProperty('exit_timeout_count')) {
                obj['exit_timeout_count'] = ApiClient.convertToType(data['exit_timeout_count'], 'Number');
            }
        }
        return obj;
    }


}

/**
 * @member {Number} buy
 */
UnfilledTimeout.prototype['buy'] = undefined;

/**
 * @member {Number} sell
 */
UnfilledTimeout.prototype['sell'] = undefined;

/**
 * @member {String} unit
 */
UnfilledTimeout.prototype['unit'] = undefined;

/**
 * @member {Number} exit_timeout_count
 */
UnfilledTimeout.prototype['exit_timeout_count'] = undefined;






export default UnfilledTimeout;

