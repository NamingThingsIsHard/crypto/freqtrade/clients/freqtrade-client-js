/**
 * Freqtrade API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 2021.12-dev21
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The ResultMsg model module.
 * @module model/ResultMsg
 * @version 2021.12-dev21
 */
class ResultMsg {
    /**
     * Constructs a new <code>ResultMsg</code>.
     * @alias module:model/ResultMsg
     * @param result {String} 
     */
    constructor(result) { 
        
        ResultMsg.initialize(this, result);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, result) { 
        obj['result'] = result;
    }

    /**
     * Constructs a <code>ResultMsg</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/ResultMsg} obj Optional instance to populate.
     * @return {module:model/ResultMsg} The populated <code>ResultMsg</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new ResultMsg();

            if (data.hasOwnProperty('result')) {
                obj['result'] = ApiClient.convertToType(data['result'], 'String');
            }
        }
        return obj;
    }


}

/**
 * @member {String} result
 */
ResultMsg.prototype['result'] = undefined;






export default ResultMsg;

