/**
 * Freqtrade API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 2021.12-dev21
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The SysInfo model module.
 * @module model/SysInfo
 * @version 2021.12-dev21
 */
class SysInfo {
    /**
     * Constructs a new <code>SysInfo</code>.
     * @alias module:model/SysInfo
     * @param cpuPct {Array.<Number>} 
     * @param ramPct {Number} 
     */
    constructor(cpuPct, ramPct) { 
        
        SysInfo.initialize(this, cpuPct, ramPct);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, cpuPct, ramPct) { 
        obj['cpu_pct'] = cpuPct;
        obj['ram_pct'] = ramPct;
    }

    /**
     * Constructs a <code>SysInfo</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/SysInfo} obj Optional instance to populate.
     * @return {module:model/SysInfo} The populated <code>SysInfo</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new SysInfo();

            if (data.hasOwnProperty('cpu_pct')) {
                obj['cpu_pct'] = ApiClient.convertToType(data['cpu_pct'], ['Number']);
            }
            if (data.hasOwnProperty('ram_pct')) {
                obj['ram_pct'] = ApiClient.convertToType(data['ram_pct'], 'Number');
            }
        }
        return obj;
    }


}

/**
 * @member {Array.<Number>} cpu_pct
 */
SysInfo.prototype['cpu_pct'] = undefined;

/**
 * @member {Number} ram_pct
 */
SysInfo.prototype['ram_pct'] = undefined;






export default SysInfo;

