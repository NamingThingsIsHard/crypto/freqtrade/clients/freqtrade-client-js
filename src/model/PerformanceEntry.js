/**
 * Freqtrade API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 2021.12-dev21
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The PerformanceEntry model module.
 * @module model/PerformanceEntry
 * @version 2021.12-dev21
 */
class PerformanceEntry {
    /**
     * Constructs a new <code>PerformanceEntry</code>.
     * @alias module:model/PerformanceEntry
     * @param pair {String} 
     * @param profit {Number} 
     * @param profitRatio {Number} 
     * @param profitPct {Number} 
     * @param profitAbs {Number} 
     * @param count {Number} 
     */
    constructor(pair, profit, profitRatio, profitPct, profitAbs, count) { 
        
        PerformanceEntry.initialize(this, pair, profit, profitRatio, profitPct, profitAbs, count);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, pair, profit, profitRatio, profitPct, profitAbs, count) { 
        obj['pair'] = pair;
        obj['profit'] = profit;
        obj['profit_ratio'] = profitRatio;
        obj['profit_pct'] = profitPct;
        obj['profit_abs'] = profitAbs;
        obj['count'] = count;
    }

    /**
     * Constructs a <code>PerformanceEntry</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/PerformanceEntry} obj Optional instance to populate.
     * @return {module:model/PerformanceEntry} The populated <code>PerformanceEntry</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new PerformanceEntry();

            if (data.hasOwnProperty('pair')) {
                obj['pair'] = ApiClient.convertToType(data['pair'], 'String');
            }
            if (data.hasOwnProperty('profit')) {
                obj['profit'] = ApiClient.convertToType(data['profit'], 'Number');
            }
            if (data.hasOwnProperty('profit_ratio')) {
                obj['profit_ratio'] = ApiClient.convertToType(data['profit_ratio'], 'Number');
            }
            if (data.hasOwnProperty('profit_pct')) {
                obj['profit_pct'] = ApiClient.convertToType(data['profit_pct'], 'Number');
            }
            if (data.hasOwnProperty('profit_abs')) {
                obj['profit_abs'] = ApiClient.convertToType(data['profit_abs'], 'Number');
            }
            if (data.hasOwnProperty('count')) {
                obj['count'] = ApiClient.convertToType(data['count'], 'Number');
            }
        }
        return obj;
    }


}

/**
 * @member {String} pair
 */
PerformanceEntry.prototype['pair'] = undefined;

/**
 * @member {Number} profit
 */
PerformanceEntry.prototype['profit'] = undefined;

/**
 * @member {Number} profit_ratio
 */
PerformanceEntry.prototype['profit_ratio'] = undefined;

/**
 * @member {Number} profit_pct
 */
PerformanceEntry.prototype['profit_pct'] = undefined;

/**
 * @member {Number} profit_abs
 */
PerformanceEntry.prototype['profit_abs'] = undefined;

/**
 * @member {Number} count
 */
PerformanceEntry.prototype['count'] = undefined;






export default PerformanceEntry;

