/**
 * Freqtrade API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 2021.12-dev21
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */


import ApiClient from "../ApiClient";
import DeleteLockRequest from '../model/DeleteLockRequest';
import HTTPValidationError from '../model/HTTPValidationError';
import Locks from '../model/Locks';

/**
* Locks service.
* @module ftclient/LocksApi
* @version 2021.12-dev21
*/
export default class LocksApi {

    /**
    * Constructs a new LocksApi. 
    * @alias module:ftclient/LocksApi
    * @class
    * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
    * default to {@link module:ApiClient#instance} if unspecified.
    */
    constructor(apiClient) {
        this.apiClient = apiClient || ApiClient.instance;
    }



    /**
     * Delete Lock
     * @param {Number} lockid 
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/Locks} and HTTP response
     */
    deleteLockApiV1LocksLockidDeleteWithHttpInfo(lockid) {
      let postBody = null;
      // verify the required parameter 'lockid' is set
      if (lockid === undefined || lockid === null) {
        throw new Error("Missing the required parameter 'lockid' when calling deleteLockApiV1LocksLockidDelete");
      }

      let pathParams = {
        'lockid': lockid
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['HTTPBasic', 'OAuth2PasswordBearer'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = Locks;
      return this.apiClient.callApi(
        '/api/v1/locks/{lockid}', 'DELETE',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Delete Lock
     * @param {Number} lockid 
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/Locks}
     */
    deleteLockApiV1LocksLockidDelete(lockid) {
      return this.deleteLockApiV1LocksLockidDeleteWithHttpInfo(lockid)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Delete Lock Pair
     * @param {module:model/DeleteLockRequest} deleteLockRequest 
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/Locks} and HTTP response
     */
    deleteLockPairApiV1LocksDeletePostWithHttpInfo(deleteLockRequest) {
      let postBody = deleteLockRequest;
      // verify the required parameter 'deleteLockRequest' is set
      if (deleteLockRequest === undefined || deleteLockRequest === null) {
        throw new Error("Missing the required parameter 'deleteLockRequest' when calling deleteLockPairApiV1LocksDeletePost");
      }

      let pathParams = {
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['HTTPBasic', 'OAuth2PasswordBearer'];
      let contentTypes = ['application/json'];
      let accepts = ['application/json'];
      let returnType = Locks;
      return this.apiClient.callApi(
        '/api/v1/locks/delete', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Delete Lock Pair
     * @param {module:model/DeleteLockRequest} deleteLockRequest 
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/Locks}
     */
    deleteLockPairApiV1LocksDeletePost(deleteLockRequest) {
      return this.deleteLockPairApiV1LocksDeletePostWithHttpInfo(deleteLockRequest)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Locks
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/Locks} and HTTP response
     */
    locksApiV1LocksGetWithHttpInfo() {
      let postBody = null;

      let pathParams = {
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['HTTPBasic', 'OAuth2PasswordBearer'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = Locks;
      return this.apiClient.callApi(
        '/api/v1/locks', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Locks
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/Locks}
     */
    locksApiV1LocksGet() {
      return this.locksApiV1LocksGetWithHttpInfo()
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


}
